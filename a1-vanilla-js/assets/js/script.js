const fName = document.querySelector('#fName');
const MI = document.querySelector('#MI');
const lName = document.querySelector('#lName');
const age = document.querySelector('#age');
const gender = document.querySelector('#gender');
const add = document.querySelector('#add');

document.querySelector('button').addEventListener('click', () => {
	fName.innerText = prompt('what is your first name?');
	MI.innerText = prompt('what is your middle initial?');
	lName.innerText = prompt('what is your last name?');
	age.innerText = prompt('How old are you?');
	gender.innerText = prompt('what is your gender?');
	add.innerText = prompt('Where do you live?');
});

document.querySelector('a').classList.add('btn-primary');
document.querySelector('a').classList.add('btn');
